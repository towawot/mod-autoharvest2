scriptname HarvestHelper
 
; ---- Core Feature ----
bool Function InitDLL(float[] mainSettings, int[] items, float[] value1, int[] items2, float[] value2, int[] items3, float[] value3, int[] items4, float[] value4) global native
bool Function GetCloseReferences(float afRadius, bool HasKwd) global native
int Function GetNumType(ObjectReference ref) global native

Function _GetGameData(int idx) global native
Function AH2_PrepareMerchantContainer() global
	_GetGameData(1)
endFunction
Function AH2_PrepareArrowData() global
	_GetGameData(2)
endFunction

; ---- Sound ----
Sound Function _HarvestSound(form baseForm) global native
Function AH2_PrepareEmptySounds() global
	_GetGameData(3)
endFunction

; ---- Block List ----
Function ListRevert(int idx) global native
Function ArrowListRevert() global
	ListRevert(1)
endFunction
Function ReferenceOnceListRevert() global
	ListRevert(2)
endFunction

; bool Function IsInBlockList(form akForm) global native


; ---- Quest Item Glow----
Function ItemGlow(int idx, ObjectReference ref) global native
Function GlowStarted(ObjectReference ref) global
	ItemGlow(1, ref)
endFunction
Function GlowStoped(ObjectReference ref) global
	ItemGlow(2, ref)
endFunction

bool Function IsGlowing(ObjectReference ref) global native
Function GlowListRevert() global
	ListRevert(3)
endFunction


; ---- Userlist/Exclusionlist ----
int Function FormlistToArray(formlist list, form[] formArray) global native
int Function FormlistToNameArray(formlist list, form[] formArray) global native

Function ListCreate(int idx, formlist list) global native
Function UserlistCreate(formlist list) global
	ListCreate(1, list)
endFunction
Function ExclusionlistCreate(formlist list) global
	ListCreate(2, list)
endFunction
Function LocationlistCreate(formlist list) global
	ListCreate(3, list)
endFunction

Function ListAdd(int idx, form akForm) global native
Function UserlistAdd(form akForm) global
	ListAdd(1, akForm)
endFunction
Function ExclusionlistAdd(form akForm) global
	ListAdd(2, akForm)
endFunction
Function LocationlistAdd(form akForm) global
	ListAdd(3, akForm)
endFunction
Function ReferenceOnceListAdd(form akForm) global
	ListAdd(4, akForm)
endFunction

Function ListRemove(int idx, form akForm) global native
Function UserlistRemove(form akForm) global
	ListRemove(1, akForm)
endFunction
Function ExclusionlistRemove(form akForm) global
	ListRemove(2, akForm)
endFunction
Function LocationlistRemove(form akForm) global
	ListRemove(3, akForm)
endFunction
Function ReferenceOnceListRemove(form akForm) global
	ListRemove(4, akForm)
endFunction

; ---- Console ----
ObjectReference Function _GetConsoleRef() global native

; ---- Debug ----
bool Function IsLoadedFISS() global native
bool Function DetectHarvestHelper() global native
Function PapyrusTrace(string str) global native
Function PapyrusTrace2(string str) global native
