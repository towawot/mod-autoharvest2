Scriptname autoHarvest2_Spool_Actor extends ObjectReference

Import HarvestHelper
ObjectReference spoolContainer

Event OnInit()
	spoolContainer = Game.GetFormFromFile(0x03E02, "AutoHarvest2.esp") as ObjectReference
endEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	sound pickUpSound = _HarvestSound(akBaseItem)
	if (pickUpSound)
		pickUpSound.Play(Game.GetPlayer())
	endif
	self.RemoveItem(akBaseItem, aiItemCount, true, spoolContainer)
endEvent
