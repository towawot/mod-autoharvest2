Scriptname autoHarvest2_Alias extends ReferenceAlias

import HarvestHelper

Actor playerRef
int lockUpdate
string sAddItemtoInventory
bool hotkeyStop
int hotkeyCount

formlist Userlist
formlist Exclusionlist
formlist LocationList

Actor spoolActorRef
Actor spoolActorGreenThumbRef
Perk GreenThumbPerk

Keyword spellKeyword

AutoHarvest2_mcm Property MCM Auto

Event OnInit()
	playerRef = Game.GetPlayer()
	spoolActorRef = Game.GetFormFromFile(0x03E00, "AutoHarvest2.esp") as Actor
	spoolActorGreenThumbRef = Game.GetFormFromFile(0x04366, "AutoHarvest2.esp") as Actor
	Userlist = Game.GetFormFromFile(0x053AD, "AutoHarvest2.esp") as Formlist
	Exclusionlist = Game.GetFormFromFile(0x05910, "AutoHarvest2.esp") as Formlist
	LocationList = Game.GetFormFromFile(0x07ECB, "AutoHarvest2.esp") as Formlist
	sAddItemtoInventory = Game.GetGameSettingString("sAddItemtoInventory")
	spellKeyword = Game.GetFormFromFile(0x09458, "AutoHarvest2.esp") as Keyword

	AH2_PrepareMerchantContainer()
	AH2_PrepareArrowData()
	AH2_PrepareEmptySounds()
	
endEvent

Event OnPlayerLoadGame()
	playerRef = Game.GetPlayer()
	spoolActorRef = Game.GetFormFromFile(0x03E00, "AutoHarvest2.esp") as Actor
	spoolActorGreenThumbRef = Game.GetFormFromFile(0x04366, "AutoHarvest2.esp") as Actor
	Userlist = Game.GetFormFromFile(0x053AD, "AutoHarvest2.esp") as Formlist
	Exclusionlist = Game.GetFormFromFile(0x05910, "AutoHarvest2.esp") as Formlist
	LocationList = Game.GetFormFromFile(0x07ECB, "AutoHarvest2.esp") as Formlist
	sAddItemtoInventory = Game.GetGameSettingString("sAddItemtoInventory")
	spellKeyword = Game.GetFormFromFile(0x09458, "AutoHarvest2.esp") as Keyword

	UserlistCreate(Userlist)
	ExclusionlistCreate(Exclusionlist)

	AH2_PrepareMerchantContainer()
	AH2_PrepareArrowData()
	AH2_PrepareEmptySounds()

	Update()

endEvent

Function Update()
; 	PapyrusTrace("Function UpdateStarter")

	ArrowListRevert()

	ReferenceOnceListRevert()

	GlowListRevert()

	UnregisterForAllKeys()
	if (MCM.mainSetting[1] && (MCM.mainSetting[2] as int) != 0)
		RegisterForKey(MCM.mainSetting[2] as int)
	endif

	bool IsFeatureEnable = (MCM.mainSetting[0] || MCM.mainSetting[9] || MCM.mainSetting[10] || MCM.mainSetting[22]) as bool
	if (!hotkeyStop && IsFeatureEnable)
		RegisterForSingleUpdate(MCM.mainSetting[4])
	endif
endFunction


Event OnKeyUp(Int KeyCode, Float HoldTime)
; 	PapyrusTrace("Function OnKeyUp code: " + KeyCode)

	if (MCM.mainSetting[1] && keyCode != 0)
		if (keyCode == MCM.mainSetting[2] as int)
			bool bDialogueMenu  = UI.IsMenuOpen("Dialogue Menu")
		
			if (bDialogueMenu || Utility.IsInMenuMode())
				bool bInventoryMenu = UI.IsMenuOpen("InventoryMenu") as bool
				bool bContainerMenu = UI.IsMenuOpen("ContainerMenu") as bool
				bool bConsole = UI.IsMenuOpen("Console") as bool
				
				if(bInventoryMenu || bContainerMenu)
					if (HoldTime < 1.0 && UserList)
						RegisterForModEvent("UserList", "UserListEvent")

						int formID = 0
						if (bInventoryMenu)
							formID = UI.GetInt("InventoryMenu", "_root.Menu_mc.inventoryLists.itemList.selectedEntry.formId")
						elseif (bContainerMenu)
							formID = UI.GetInt("ContainerMenu", "_root.Menu_mc.inventoryLists.itemList.selectedEntry.formId")
						endif
						if (formID)
							Form Item = Game.GetFormEx(formID)
							if (Item)
								SendModEvent("UserList", formID as String)
							endif
						endif
						UnregisterForModEvent("UserList")
					elseif (Exclusionlist)
						RegisterForModEvent("Exclusionlist", "ExclusionlistEvent")

						int formID = 0
						if (bInventoryMenu)
							formID = UI.GetInt("InventoryMenu", "_root.Menu_mc.inventoryLists.itemList.selectedEntry.formId")
						elseif (bContainerMenu)
							formID = UI.GetInt("ContainerMenu", "_root.Menu_mc.inventoryLists.itemList.selectedEntry.formId")
						endif

						if (formID)
							Form Item = Game.GetFormEx(formID)
							if (Item)
								SendModEvent("Exclusionlist", formID as String)
							endif
						endif
						UnregisterForModEvent("Exclusionlist")
					endif
				elseif (bConsole)
					if (HoldTime > 1.0 && Exclusionlist)
						ObjectReference targetRef = _GetConsoleRef()
						if (targetRef)
							form baseForm = targetRef.GetBaseObject()
							if (baseForm && baseForm as Container)
								if (!Exclusionlist.HasForm(targetRef))
									Debug.Notification("$AH_registExclusionlist")
									Exclusionlist.AddForm(targetRef)
									ExclusionlistAdd(targetRef)
									ReferenceOnceListRevert()
								else
									Debug.Notification("$AH_unregistExclusionlist")
									Exclusionlist.RemoveAddedForm(targetRef)
									ExclusionlistRemove(targetRef)
									ReferenceOnceListRevert()
								endif
							endif
						endif
					endif
				endif
			else
				hotkeyStop = !hotkeyStop
				if (hotkeyStop)
					Debug.Notification("$AH_pausedAutoHarvest")
				else
					Debug.Notification("$AH_restartAutoHarvest")
					Update()
				endif
			endif
		endif
	endif
EndEvent

; Event BlockLocationListEvent(string eventName, string ItemName, float fFormID, Form sender)
; 	if (sender == self.GetOwningQuest())
; 		int formID = fFormID as int
; 		form Item = Game.GetFormEx(formID)
; 		if (Item)
; 			if (!BlockLocationlist.HasForm(Item))
; 				Debug.Notification("$AH_registUserlist")
; 				BlockLocationlist.AddForm(Item)
; 				BlockLocationlistAdd(Item)
; 			else
; 				Debug.Notification("$AH_unregistUserlist")
; 				BlockLocationlist.RemoveAddedForm(Item)
; 				BlockLocationlistRemove(Item)
; 			endif
; ; 			Debug.Notification("GetSize:" + userlist.GetSize())
; 		endif
; 	endif
; endEvent

Event UserListEvent(string eventName, string ItemID, float fFormID, Form sender)
	if (sender == self.GetOwningQuest())
		int formID = ItemID as int
		if (formID)
			form Item = Game.GetFormEx(formID)
			if (Item)
				if (!Userlist.HasForm(Item))
					Debug.Notification("$AH_registUserlist")
					userlist.AddForm(Item)
					UserlistAdd(Item)
					ReferenceOnceListRevert()
				else
					Debug.Notification("$AH_unregistUserlist")
					userlist.RemoveAddedForm(Item)
					UserlistRemove(Item)
					ReferenceOnceListRevert()
				endif
	; 			Debug.Notification("GetSize:" + userlist.GetSize())
			endif
		endif
	endif
endEvent

Event ExclusionlistEvent(string eventName, string ItemID, float fFormID, Form sender)
	if (sender == self.GetOwningQuest())
		int formID = ItemID as int
		if (formID)
			form Item = Game.GetFormEx(formID)
			if (Item)
				if (!Exclusionlist.HasForm(Item))
					Debug.Notification("$AH_registExclusionlist")
					Exclusionlist.AddForm(Item)
					ExclusionlistAdd(Item)
					ReferenceOnceListRevert()
				else
					Debug.Notification("$AH_unregistExclusionlist")
					Exclusionlist.RemoveAddedForm(Item)
					ExclusionlistRemove(Item)
					ReferenceOnceListRevert()
				endif
	; 			Debug.Notification("GetSize:" + userlist.GetSize())
			endif
		endif
	endif
endEvent

Event OnUpdate()
; 	PapyrusTrace("Event OnUpdate lockUpdate: " + lockUpdate)

	bool doLoop = true

	if (lockUpdate)
		return
	elseif (!MCM.loadDll)
		Debug.Notification("$AH_unloadDLL")
		return
	elseif (MCM.mainSetting[13])
		if (MCM.mainSetting[13] == 1.0)
			bool InCombat = playerRef.IsInCombat()
			if (InCombat)
				doLoop = false
			endif
		elseif (MCM.mainSetting[13] == 2.0)
			bool WeaponDrawn = playerRef.IsWeaponDrawn()
			if (WeaponDrawn)
				doLoop = false
			endif
		endif
	endif

	bool AllowUpdate1 = (MCM.mainSetting[0] || MCM.mainSetting[9] || MCM.mainSetting[10])
	bool AllowUpdate2 = (MCM.mainSetting[22])
	bool castSpell = playerRef.HasEffectKeyword(spellKeyword)

	if (doLoop)
		lockUpdate += 1

		bool IsInMenu = Utility.IsInMenumode()
		if (IsInMenu && (AllowUpdate1 || AllowUpdate2))
			lockUpdate = 0
			RegisterForSingleUpdate(MCM.mainSetting[24])
			return
		endif

		if (!hotkeyStop)

			bool result = false

			if (castSpell)
				result = GetCloseReferences(MCM.mainSetting[23], castSpell)
			else
				result = GetCloseReferences(MCM.mainSetting[3], castSpell)
			endif

			if (hotkeyCount as bool)
				hotkeyCount = 0
			endif
		else
			hotkeyCount += 1
			if (hotkeyCount >= 10)
				if (!MCM.mainSetting[14])
					Debug.Notification("$AH_pausedAutoHarvest")
				endif
				hotkeyCount = 0
			endif
		endif
		
		lockUpdate = 0
	endif

	if (AllowUpdate1 || AllowUpdate2)
		if (castSpell)
			RegisterForSingleUpdate(MCM.mainSetting[24])
		else
			if (AllowUpdate1)
				RegisterForSingleUpdate(MCM.mainSetting[4])
			elseif (AllowUpdate2)
				RegisterForSingleUpdate(MCM.mainSetting[24])
			endif
		endif
	endif
endEvent

Event OnAutoHarvest(ObjectReference ref, int objType, int actiType, bool questObject, bool HasGreenThumb, bool display)
; 	PapyrusTrace("Event OnAutoHarvest " + ref.GetDisplayName())
	bool IsHarvestable = (actiType == 1 || actiType == 2) as bool
	if (ref && IsHarvestable)
		bool IsHasGreenThumbTarget = (objType == 0 || objType == 1 || objType == 3) as bool
		bool IsOreVein = (objType == 35) as bool

		if (questObject)
			ref.Activate(playerRef)
		elseif (IsOreVein)
			if (ref as MineOreScript)
				if ((ref as MineOreScript).ResourceCountCurrent != 0) 
					(ref as MineOreScript).giveOre()
				else
	; 				Debug.Notification("List Added:" + ref.GetDisplayName())
					ReferenceOnceListAdd(ref)
				endif
			else
; 				Debug.Notification("List Added:" + ref.GetDisplayName())
				ReferenceOnceListAdd(ref)
			endif
		elseif (display)
			if (actiType == 1)
				ref.Activate(playerRef)
			else
				playerRef.addItem(ref);, abSilent = true)
			endif
		else
			if (actiType == 1)
				if (IsHasGreenThumbTarget && HasGreenThumb)
					ref.Activate(spoolActorGreenThumbRef)
				else
					ref.Activate(spoolActorRef)
				endif
			else
				if (IsHasGreenThumbTarget && HasGreenThumb)
					spoolActorGreenThumbRef.addItem(ref, abSilent = true)
				else
					spoolActorRef.addItem(ref, abSilent = true)
				endif
			endif
		endif
	endif
endEvent

Event OnLootContainer(ObjectReference containerRef, form akForm, int count, sound pickUpSound, int objType, bool display)
; 	PapyrusTrace("Event OnLootContainer cont:" + containerRef.GetDisplayName() + " form:"+ akForm.GetName() + " count:"+count)

	if (count > 0)
		if (containerRef && akForm)
			containerRef.RemoveItem(akForm, count, true, spoolActorRef)

			if (display)
				Debug.Notification(akForm.GetName() + " (" + count + ") " + sAddItemtoInventory)
			endif
		endif
	endif
endEvent

Event OnLootDeadBody(ObjectReference actorRef, form akForm, int count, sound pickUpSound, int objType, bool display)
; 	PapyrusTrace("Event OnLootDeadBody cont:" + actorRef.GetDisplayName() + " form:"+ akForm.GetName() + " count:"+count)

	if (count > 0)
		if (actorRef && akForm)
			actorRef.RemoveItem(akForm, count, true, spoolActorRef)

			if (display)
				Debug.Notification(akForm.GetName() + " (" + count + ") " + sAddItemtoInventory)
			endif
		endif
	endif
endEvent

Event OnGlowQuestItem(ObjectReference ref, int objType)
; 	PapyrusTrace("Event OnQuestItemFound ref:" + " ref:"+ ref.GetDisplayName() + " objType:"+ objType)

	if (ref)
		bool IsGlow = IsGlowing(ref)
		if (!IsGlow)
			GlowStarted(ref)
			EffectShader shader = Game.GetFormFromFile(0x05E75, "AutoHarvest2.esp") as EffectShader
			if (shader)
				shader.Play(ref, 0.5)

; 			test code 
; 			ObjectReference pillarRef = Game.GetFormFromFile(0x0693F, "AutoHarvest2.esp") as ObjectReference
; 			if (pillarRef && (pillarRef.GetParentCell() != ref.GetParentCell()))
; 				if(playerRef.GetDistance(pillarRef) > MCM.mainSetting[3])
; 					pillarRef.MoveTo(ref, abMatchRotation = false)
; 					pillarRef.SetPosition(ref.x, ref.y, ref.z)
; 				endif
; 			endif
				utility.wait(2.5)
			endif
; 		ObjectReference pillarRef = Game.GetFormFromFile(0x0693F, "AutoHarvest2.esp") as ObjectReference
; 		if(pillarRef && playerRef.GetDistance(pillarRef) > MCM.mainSetting[3])
; 			ObjectReference pillarMarker = Game.GetFormFromFile(0x06941, "AutoHarvest2.esp") as ObjectReference
; 			pillarRef.MoveTo(pillarMarker, abMatchRotation = false)
; 		endif
			GlowStoped(ref)
		endif
	endif
endEvent

Event OnDebugGlowMode(ObjectReference ref)
; 	PapyrusTrace("Event OnDebugGlowMode ref:" + " ref:"+ ref.GetDisplayName())

	bool IsGlow = IsGlowing(ref)
	if (!IsGlow)
		if (ref)
			GlowStarted(ref)
			EffectShader shader = Game.GetFormFromFile(0x05E76, "AutoHarvest2.esp") as EffectShader
			if (shader && ref)
				shader.Play(ref, 0.5)
				utility.wait(2.5)
			endif
			GlowStoped(ref)
		endif
	endif
endEvent

Event OnContainerAnimation(ObjectReference ref, int mode)
; 	PapyrusTrace("Event OnContainerAnimation" + ref.GetDisplayName())

	if (ref)
		bool Is3DLoad = ref.Is3dLoaded()
		if (Is3DLoad)
			MiscObject misc = Game.GetFormFromFile(0x0842E, "AutoHarvest2.esp") as MiscObject
			bool HasFlagObject = !ref.GetItemCount(misc) as bool
			if (HasFlagObject)
				bool Result = ref.PlayGamebryoAnimation("Open", true)
				if (Result)
					ref.AddItem(misc)
				endif
			endif
		endif
	endif
endEvent

Event OnLocationChange(Location akOldLoc, Location akNewLoc)
; 	PapyrusTrace("Event OnLocationChange")

	ArrowListRevert()
	
	ReferenceOnceListRevert()

	GlowListRevert()
	
endEvent
