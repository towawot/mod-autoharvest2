Scriptname AutoHarvest2_HarvestSpell extends activemagiceffect  

import HarvestHelper

Event OnEffectStart(Actor aTarget, Actor akCaster)
	if akCaster == Game.GetPlayer()
		ReferenceOnceListRevert()
	endif
endEvent


Event OnEffectFinish(Actor akTarget, Actor akCaster)
	if akCaster == Game.GetPlayer()
		ReferenceOnceListRevert()
	endif
endEvent
