Scriptname AutoHarvest2_Spool_GreenThumbActor extends ObjectReference

Import HarvestHelper
ObjectReference spoolContainer
Perk GreenThumbPerk
float GreenThumbPerkValue

Event OnInit()
	spoolContainer = Game.GetFormFromFile(0x03E02, "AutoHarvest2.esp") as ObjectReference
	GreenThumbPerk = Game.GetFormFromFile(0x105F2E, "Skyrim.esm") as Perk
	if (GreenThumbPerk)
		GreenThumbPerkValue = GreenThumbPerk.GetNthEntryValue(0, 0)
	endif
endEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	int modItemCount = 0
	if (GreenThumbPerk)
		modItemCount = ((aiItemCount * GreenThumbPerkValue) - aiItemCount) as int
		spoolContainer.AddItem(akBaseItem, modItemCount, true)
	endif

	sound pickUpSound = _HarvestSound(akBaseItem)
	if (pickUpSound)
		pickUpSound.Play(Game.GetPlayer())
	endif
	self.RemoveItem(akBaseItem, aiItemCount, true, spoolContainer)
endEvent

