Scriptname AutoHarvest2_mcm extends SKI_ConfigBase  

Import HarvestHelper
Import Game

autoHarvest2_Alias Property MQ Auto

bool property loadDll auto

int[] ID_mainSetting
float[] Property mainSetting Auto

int[] ID_Switch
int[] Property itemSwitch Auto
int[] ID_switchOption1
float[] Property switchOption1 Auto

int[] ID_Switch2
int[] Property itemSwitch2 Auto
int[] ID_switchOption2
float[] Property switchOption2 Auto

int[] ID_Switch3
int[] Property itemSwitch3 Auto
int[] ID_switchOption3
float[] Property switchOption3 Auto

int[] ID_Switch4
int[] Property itemSwitch4 Auto
int[] ID_switchOption4
float[] Property switchOption4 Auto

Form[] userlistItems
int[]  ID_Userlist
bool[] userlistFlag

Form[] exclusionlistItems
int[]  ID_Exclusionlist
bool[] exclusionlistFlag

Form[] locationlistItems
int[]  ID_Locationlist
bool[] locationlistFlag

int[] ID_Fiss
int[] Property fissSetting Auto

int[] ID_FissPresetNumber
float[] Property fissPresetNumber Auto

String[] s_mainSetting
String[] s1_mainSetting
String[] s_mainSettingDesc
String[] s_itemCategory
String[] s_switchOption
String[] s1_switchOption
String[] s_fissSettingDesc
String[] s1_fissPresetNumber
String[] s_itemCategoryDesc

String[] _switchText
String[] _switchOleVeinText
String[] _switchAllText
String[] _ownershipText
String[] _questItemText
String[] _inCombatText
String[] _openContainerText
String[] _registerText

formlist Userlist
formlist Exclusionlist
formlist LocationList
location _playerLoc
cell _playerCell

Event OnConfigInit()
	ModName = "AutoHarvest2"

	Pages = New String[9]
	Pages[0] = "$AH_generalP"
	Pages[1] = "$AH_optionP"
	Pages[2] = "$AH_autoharvestP"
	Pages[3] = "$AH_containerP"
	Pages[4] = "$AH_deadbodiesP"
	Pages[5] = "$AH_harvestSpellP"
	Pages[6] = "$AH_userListP"
 	Pages[7] = "$AH_exclusionListP"
 	Pages[8] = "$AH_locationListP"

	ID_mainSetting = New Int[40]
	mainSetting = New float[40]
	mainSetting[3] = 300
	mainSetting[4] = 0.3
	mainSetting[5] = 1.0
	mainSetting[6] = 2.0
	mainSetting[7] = 1.0
	mainSetting[12] = 1.0
	mainSetting[15] = 1.0
	mainSetting[20] = 1.0
	mainSetting[21] = 1.0
	mainSetting[23] = 5000.0
	mainSetting[24] = 1.0

	ID_switch = New Int[40]
	itemSwitch = New Int[40]

	ID_switch2 = New Int[40]
	itemSwitch2 = New Int[40]

	ID_switch3 = New Int[40]
	itemSwitch3 = New Int[40]
	
	ID_switch4 = New Int[40]
	itemSwitch4 = New Int[40]

	ID_switchOption1 = New Int[40]
	switchOption1 = New float[40]

	ID_switchOption2 = New Int[40]
	switchOption2 = New float[40]

	ID_switchOption3 = New Int[40]
	switchOption3 = New float[40]

	ID_switchOption4 = New Int[40]
	switchOption4 = New float[40]
	
	ID_Fiss = New Int[2]
	fissSetting = New Int[2]

	ID_FissPresetNumber = New int[1]
	fissPresetNumber = New float[1]
	fissPresetNumber[0] = 0.0

	s1_fissPresetNumber = New String[1]
	s1_fissPresetNumber[0] = "${0}"
	
; 	userlistFlag = New bool[127]
; 	ID_Userlist = New int[127]

	s_fissSettingDesc = New String[2]
	s_fissSettingDesc[0]  = "$AH_savePersonalPresetDesc"
	s_fissSettingDesc[1]  = "$AH_loadPersonalPresetDesc"

	s_mainSetting = New String[40]
	s_mainSetting[0]  = "$AH_autoharvest"
	s_mainSetting[1]  = "$AH_enableHotkey"
	s_mainSetting[2]  = "$AH_hotkeyCode"
	s_mainSetting[3]  = "$AH_radius"
	s_mainSetting[4]  = "$AH_updateInterval"
	s_mainSetting[5]  = "$AH_owershipCheck"
	s_mainSetting[6]  = "$AH_questItemCheck"
	s_mainSetting[7]  = "$AH_exceptReadBook"
	s_mainSetting[8]  = "$AH_LOSCheck"
	s_mainSetting[9]  = "$AH_container"
	s_mainSetting[10] = "$AH_deadbodies"
	s_mainSetting[11] = "$AH_individualSettings"
	s_mainSetting[12] = "$AH_exceptChestModel"
	s_mainSetting[13] = "$AH_inCombatCheck"
	s_mainSetting[14] = "$AH_disableBlockNotification"
	s_mainSetting[15] = "$AH_summonableActor"
	s_mainSetting[16] = "$AH_debugMode"
	s_mainSetting[17] = "$AH_blockLocation"
	s_mainSetting[18] = "$AH_blockCell"
	s_mainSetting[19] = "$AH_openContainer"
	s_mainSetting[20] = "$AH_blockPlayerHouse"
	s_mainSetting[21] = "$AH_owershipSneakCheck"
	s_mainSetting[22] = "$AH_harvestSpell"
	s_mainSetting[23] = "$AH_radius"
	s_mainSetting[24] = "$AH_updateInterval"

	s_mainSettingDesc = New String[40]
	s_mainSettingDesc[0]  = "$AH_autoharvestDesc"
	s_mainSettingDesc[1]  = "$AH_hotkeyDesc"
	s_mainSettingDesc[2]  = "$AH_hotkeyDesc"
	s_mainSettingDesc[3]  = "$AH_radiusDesc"
	s_mainSettingDesc[4]  = "$AH_updateIntervalDesc"
	s_mainSettingDesc[5]  = "$AH_owershipCheckDesc"
	s_mainSettingDesc[6]  = "$AH_questItemCheckDesc"
	s_mainSettingDesc[7]  = "$AH_exceptReadBookDesc"
	s_mainSettingDesc[8]  = "$AH_LOSCheckDesc"
	s_mainSettingDesc[9]  = "$AH_containerDesc"
	s_mainSettingDesc[10] = "$AH_deadbodiesDesc"
	s_mainSettingDesc[11] = "$AH_individualSettingsDesc"
	s_mainSettingDesc[12] = "$AH_exceptChestModelDesc"
	s_mainSettingDesc[13] = "$AH_inCombatCheckDesc"
	s_mainSettingDesc[14] = "$AH_disableBlockNotificationDesc"
	s_mainSettingDesc[15] = "$AH_summonableActorDesc"
	s_mainSettingDesc[16] = "$AH_debugModeDesc"
	s_mainSettingDesc[17] = "$AH_blockLocationDesc"
	s_mainSettingDesc[18] = "$AH_blockCellDesc"
	s_mainSettingDesc[19] = "$AH_openContainerDesc"
	s_mainSettingDesc[20] = "$AH_blockPlayerHouseDesc"
	s_mainSettingDesc[21] = "$AH_owershipCheckDesc"
	s_mainSettingDesc[22] = "$AH_harvestSpellDesc"
	s_mainSettingDesc[23] = "$AH_radiusDesc"
	s_mainSettingDesc[22] = "$AH_harvestSpellDesc"
	s_mainSettingDesc[23] = "$AH_radiusDesc"
	s_mainSettingDesc[24] = "$AH_updateIntervalDesc"

	s1_mainSetting = New String[40]
	s1_mainSetting[3] = "${0}"
	s1_mainSetting[4] = "${1}sec"
	s1_mainSetting[23] = "${0}"
	s1_mainSetting[24] = "${1}sec"

	s_itemCategory = New String[40]
	s_itemCategory[0]  = "$AH_flora"
	s_itemCategory[1]  = "$AH_fieldCrop"
	s_itemCategory[2]  = "$AH_critter"
	s_itemCategory[3]  = "$AH_otherFloraTree"
	s_itemCategory[4]  = "$AH_ingredient"
	s_itemCategory[5]  = "$AH_septim"
	s_itemCategory[6]  = "$AH_gem"
	s_itemCategory[7]  = "$AH_lockpick"
	s_itemCategory[8]  = "$AH_animalHide"
	s_itemCategory[9]  = "$AH_animalPart"
	s_itemCategory[10] = "$AH_oreIngot"
	s_itemCategory[11] = "$AH_soulGem"
	s_itemCategory[12] = "$AH_key"
	s_itemCategory[13] = "$AH_clutter"
	s_itemCategory[14] = "$AH_dwemerObject"
	s_itemCategory[15] = "$AH_brokenObject"
	s_itemCategory[16] = "$AH_book"
	s_itemCategory[17] = "$AH_spellbook"
	s_itemCategory[18] = "$AH_skillbook"
	s_itemCategory[19] = "$AH_scroll"
	s_itemCategory[20] = "$AH_ammo"
	s_itemCategory[21] = "$AH_weapon"
	s_itemCategory[22] = "$AH_enchantedWeapon"
	s_itemCategory[23] = "$AH_armor"
	s_itemCategory[24] = "$AH_ring"
	s_itemCategory[25] = "$AH_necklace"
	s_itemCategory[26] = "$AH_enchantedArmor"
	s_itemCategory[27] = "$AH_enchantedRing"
	s_itemCategory[28] = "$AH_enchantedNecklace"
	s_itemCategory[29] = "$AH_potion"
	s_itemCategory[30] = "$AH_poison"
	s_itemCategory[31] = "$AH_food"
	s_itemCategory[32] = "$AH_foodIngredient"
	s_itemCategory[33] = "$AH_drink"
	s_itemCategory[34] = "$AH_userList"
	s_itemCategory[35] = "$AH_OreVein"

	s_itemCategoryDesc = New String[40]
	s_itemCategoryDesc[0]  = "$AH_floraDesc"
	s_itemCategoryDesc[1]  = "$AH_fieldCropDesc"
	s_itemCategoryDesc[2]  = "$AH_critterDesc"
	s_itemCategoryDesc[3]  = "$AH_otherFloraTreeDesc"
	s_itemCategoryDesc[4]  = "$AH_ingredientDesc"
	s_itemCategoryDesc[5]  = "$AH_septimDesc"
	s_itemCategoryDesc[6]  = "$AH_gemDesc"
	s_itemCategoryDesc[7]  = "$AH_lockpickDesc"
	s_itemCategoryDesc[8]  = "$AH_animalHideDesc"
	s_itemCategoryDesc[9]  = "$AH_animalPartDesc"
	s_itemCategoryDesc[10] = "$AH_oreIngotDesc"
	s_itemCategoryDesc[11] = "$AH_soulGemDesc"
	s_itemCategoryDesc[12] = "$AH_keyDesc"
	s_itemCategoryDesc[13] = "$AH_clutterDesc"
	s_itemCategoryDesc[14] = "$AH_dwemerObjectDesc"
	s_itemCategoryDesc[15] = "$AH_brokenObjectDesc"
	s_itemCategoryDesc[16] = "$AH_bookDesc"
	s_itemCategoryDesc[17] = "$AH_spellbookDesc"
	s_itemCategoryDesc[18] = "$AH_skillbookDesc"
	s_itemCategoryDesc[19] = "$AH_scrollDesc"
	s_itemCategoryDesc[20] = "$AH_ammoDesc"
	s_itemCategoryDesc[21] = "$AH_weaponDesc"
	s_itemCategoryDesc[22] = "$AH_enchantedWeaponDesc"
	s_itemCategoryDesc[23] = "$AH_armorDesc"
	s_itemCategoryDesc[24] = "$AH_ringDesc"
	s_itemCategoryDesc[25] = "$AH_necklaceDesc"
	s_itemCategoryDesc[26] = "$AH_enchantedArmorDesc"
	s_itemCategoryDesc[27] = "$AH_enchantedRingDesc"
	s_itemCategoryDesc[28] = "$AH_enchantedNecklaceDesc"
	s_itemCategoryDesc[29] = "$AH_potionDesc"
	s_itemCategoryDesc[30] = "$AH_poisonDesc"
	s_itemCategoryDesc[31] = "$AH_foodDesc"
	s_itemCategoryDesc[32] = "$AH_foodIngredientDesc"
	s_itemCategoryDesc[33] = "$AH_drinkDesc"
	s_itemCategoryDesc[34] = "$AH_userListDesc"
	s_itemCategoryDesc[35] = "$AH_OreVeinDesc"
	
	s_switchOption = New String[40]
	s_switchOption[0]  = "$v/w"
	s_switchOption[1]  = ""
	s_switchOption[2]  = "$v/w"
	s_switchOption[3]  = "$v/w"
	s_switchOption[4]  = "$v/w"
	s_switchOption[5]  = "$v/w"
	s_switchOption[6]  = "$v/w"
	s_switchOption[7]  = ""
	s_switchOption[8]  = "$v/w"
	s_switchOption[9]  = "$v/w"
	s_switchOption[10] = "$v/w"
	s_switchOption[11] = "$v/w"
	s_switchOption[12] = ""
	s_switchOption[13] = "$v/w"
	s_switchOption[14] = "$v/w"
	s_switchOption[15] = "$v/w"
	s_switchOption[16] = "$v/w"
	s_switchOption[17] = "$v/w"
	s_switchOption[18] = "$v/w"
	s_switchOption[19] = "$v/w"
	s_switchOption[20] = "$AH_damageValue"
	s_switchOption[21] = "$v/w"
	s_switchOption[23] = "$v/w"
	s_switchOption[24] = "$v/w"
	s_switchOption[25] = "$v/w"
	s_switchOption[22] = "$v/w"
	s_switchOption[26] = "$v/w"
	s_switchOption[27] = "$v/w"
	s_switchOption[28] = "$v/w"
	s_switchOption[29] = "$v/w"
	s_switchOption[30] = "$v/w"
	s_switchOption[31] = "$v/w"
	s_switchOption[32] = "$v/w"
	s_switchOption[33] = "$v/w"
	s_switchOption[34] = ""

	s1_switchOption = New String[40]
	s1_switchOption[0]  = "$v/w{1}"
	s1_switchOption[1]  = ""
	s1_switchOption[2]  = "$v/w{1}"
	s1_switchOption[3]  = "$v/w{1}"
	s1_switchOption[4]  = "$v/w{1}"
	s1_switchOption[5]  = "$v/w{1}"
	s1_switchOption[6]  = "$v/w{1}"
	s1_switchOption[7]  = ""
	s1_switchOption[8]  = "$v/w{1}"
	s1_switchOption[9]  = "$v/w{1}"
	s1_switchOption[10] = "$v/w{1}"
	s1_switchOption[11] = "$v/w{1}"
	s1_switchOption[12] = ""
	s1_switchOption[13] = "$v/w{1}"
	s1_switchOption[14] = "$v/w{1}"
	s1_switchOption[15] = "$v/w{1}"
	s1_switchOption[16] = "$v/w{1}"
	s1_switchOption[17] = "$v/w{1}"
	s1_switchOption[18] = "$v/w{1}"
	s1_switchOption[19] = "$v/w{1}"
	s1_switchOption[20] = "$AH_damageValue{0}over"
	s1_switchOption[21] = "$v/w{1}"
	s1_switchOption[23] = "$v/w{1}"
	s1_switchOption[24] = "$v/w{1}"
	s1_switchOption[25] = "$v/w{1}"
	s1_switchOption[22] = "$v/w{1}"
	s1_switchOption[26] = "$v/w{1}"
	s1_switchOption[27] = "$v/w{1}"
	s1_switchOption[28] = "$v/w{1}"
	s1_switchOption[29] = "$v/w{1}"
	s1_switchOption[30] = "$v/w{1}"
	s1_switchOption[31] = "$v/w{1}"
	s1_switchOption[32] = "$v/w{1}"
	s1_switchOption[33] = "$v/w{1}"
	s1_switchOption[34] = ""
	s1_switchOption[35] = ""
			
	_switchText = New String[3]
	_switchText[0] = "$AH_switchOFF"
	_switchText[1] = "$AH_switchON_Silent"
	_switchText[2] = "$AH_switchON_MSG"

	_switchOleVeinText = New String[2]
	_switchOleVeinText[0] = "$AH_switchOFF"
	_switchOleVeinText[1] = "$AH_switchON_MSG"

	_ownershipText = New String[3]
	_ownershipText[0] = "$AH_ownershipNoCheck"
	_ownershipText[1] = "$AH_ownershipCrimeCheck"
	_ownershipText[2] = "$AH_ownershipOwnerCheck"

	_questItemText = New String[3]
	_questItemText[0] = "$AH_questItemLoot"
	_questItemText[1] = "$AH_questItemNoLoot"
	_questItemText[2] = "$AH_questItemNoLootGlow"

	_inCombatText = New String[3]
	_inCombatText[0] = "$AH_inCombatNone"
	_inCombatText[1] = "$AH_inCombatStop"
	_inCombatText[2] = "$AH_inCombatSheathe"

	_openContainerText = New String[3]
	_openContainerText[0] = "$AH_openContainerNone"
	_openContainerText[1] = "$AH_openAfterLooting"
	_openContainerText[2] = "$AH_openAfterChecking"
	
	_registerText = New String[2]
	_registerText[0] = "$AH_register"
	_registerText[1] = "$AH_unregister"

	_switchAllText = New String[2]
	_switchAllText[0] = "$AH_all"
	_switchAllText[1] = "$AH_all"

	Userlist = Game.GetFormFromFile(0x053AD, "AutoHarvest2.esp") as Formlist
	Exclusionlist = Game.GetFormFromFile(0x05910, "AutoHarvest2.esp") as Formlist
	LocationList = Game.GetFormFromFile(0x07ECB, "AutoHarvest2.esp") as Formlist

endEvent

int function GetVersion()
	return 8
endFunction

Event OnVersionUpdate(int a_version)
	if (a_version >= 7 && CurrentVersion < 7)
; 		script version 7
		ID_FissPresetNumber = New int[1]
		fissPresetNumber = New float[1]
		fissPresetNumber[0] = 0.0
		s1_fissPresetNumber = New String[1]
		s1_fissPresetNumber[0] = "${0}"

		Pages = New String[9]
		Pages[0] = "$AH_generalP"
		Pages[1] = "$AH_optionP"
		Pages[2] = "$AH_autoharvestP"
		Pages[3] = "$AH_containerP"
		Pages[4] = "$AH_deadbodiesP"
		Pages[5] = "$AH_harvestSpellP"
		Pages[6] = "$AH_userListP"
	 	Pages[7] = "$AH_exclusionListP"
	 	Pages[8] = "$AH_locationListP"

		ID_switch4 = New Int[40]
		itemSwitch4 = New Int[40]
		ID_switchOption4 = New Int[40]
		switchOption4 = New float[40]

		s_mainSetting[20] = "$AH_blockPlayerHouse"
		s_mainSetting[21] = "$AH_owershipSneakCheck"
		s_mainSetting[22] = "$AH_harvestSpell"
		s_mainSetting[23] = "$AH_radius"
		s_mainSetting[24] = "$AH_updateInterval"

		s_mainSettingDesc[22] = "$AH_harvestSpellDesc"
		s_mainSettingDesc[23] = "$AH_radiusDesc"
		s_mainSettingDesc[24] = "$AH_updateIntervalDesc"

		s1_mainSetting[23] = "${0}"
		s1_mainSetting[24] = "${1}sec"

		mainSetting[8] = 0.0
		mainSetting[20] = 1.0
		mainSetting[21] = 1.0
		mainSetting[23] = 5000.0
		mainSetting[24] = 1.0

		Userlist = Game.GetFormFromFile(0x053AD, "AutoHarvest2.esp") as Formlist
		Exclusionlist = Game.GetFormFromFile(0x05910, "AutoHarvest2.esp") as Formlist
		LocationList = Game.GetFormFromFile(0x07ECB, "AutoHarvest2.esp") as Formlist

; 		script version 3
		mainSetting[6] = 1.0
		_questItemText = New String[3]
		_questItemText[0] = "$AH_questItemLoot"
		_questItemText[1] = "$AH_questItemNoLoot"
		_questItemText[2] = "$AH_questItemNoLootGlow"

; 		script version 5
		s_itemCategory[35] = "$AH_OreVein"
		s1_switchOption[35] = ""

		_switchOleVeinText = New String[2]
		_switchOleVeinText[0] = "$AH_switchOFF"
		_switchOleVeinText[1] = "$AH_switchON_MSG"

; 		script version 6
		s_fissSettingDesc = New String[2]
		s_fissSettingDesc[0]  = "$AH_savePersonalPresetDesc"
		s_fissSettingDesc[1]  = "$AH_loadPersonalPresetDesc"
	endif

	if (a_version >= 8 && CurrentVersion < 8)
		s_itemCategoryDesc = New String[40]
		s_itemCategoryDesc[0]  = "$AH_floraDesc"
		s_itemCategoryDesc[1]  = "$AH_fieldCropDesc"
		s_itemCategoryDesc[2]  = "$AH_critterDesc"
		s_itemCategoryDesc[3]  = "$AH_otherFloraTreeDesc"
		s_itemCategoryDesc[4]  = "$AH_ingredientDesc"
		s_itemCategoryDesc[5]  = "$AH_septimDesc"
		s_itemCategoryDesc[6]  = "$AH_gemDesc"
		s_itemCategoryDesc[7]  = "$AH_lockpickDesc"
		s_itemCategoryDesc[8]  = "$AH_animalHideDesc"
		s_itemCategoryDesc[9]  = "$AH_animalPartDesc"
		s_itemCategoryDesc[10] = "$AH_oreIngotDesc"
		s_itemCategoryDesc[11] = "$AH_soulGemDesc"
		s_itemCategoryDesc[12] = "$AH_keyDesc"
		s_itemCategoryDesc[13] = "$AH_clutterDesc"
		s_itemCategoryDesc[14] = "$AH_dwemerObjectDesc"
		s_itemCategoryDesc[15] = "$AH_brokenObjectDesc"
		s_itemCategoryDesc[16] = "$AH_bookDesc"
		s_itemCategoryDesc[17] = "$AH_spellbookDesc"
		s_itemCategoryDesc[18] = "$AH_skillbookDesc"
		s_itemCategoryDesc[19] = "$AH_scrollDesc"
		s_itemCategoryDesc[20] = "$AH_ammoDesc"
		s_itemCategoryDesc[21] = "$AH_weaponDesc"
		s_itemCategoryDesc[22] = "$AH_enchantedWeaponDesc"
		s_itemCategoryDesc[23] = "$AH_armorDesc"
		s_itemCategoryDesc[24] = "$AH_ringDesc"
		s_itemCategoryDesc[25] = "$AH_necklaceDesc"
		s_itemCategoryDesc[26] = "$AH_enchantedArmorDesc"
		s_itemCategoryDesc[27] = "$AH_enchantedRingDesc"
		s_itemCategoryDesc[28] = "$AH_enchantedNecklaceDesc"
		s_itemCategoryDesc[29] = "$AH_potionDesc"
		s_itemCategoryDesc[30] = "$AH_poisonDesc"
		s_itemCategoryDesc[31] = "$AH_foodDesc"
		s_itemCategoryDesc[32] = "$AH_foodIngredientDesc"
		s_itemCategoryDesc[33] = "$AH_drinkDesc"
		s_itemCategoryDesc[34] = "$AH_userListDesc"
		s_itemCategoryDesc[35] = "$AH_OreVeinDesc"
	endif
endEvent

Event OnGameReload()
	parent.OnGameReload()
	ApplySetting()
endEvent

Function ApplySetting()

	loadDll = DetectHarvestHelper()
	
	if (loadDll)
		if (mainSetting[11])

			InitDLL(mainSetting, itemSwitch, switchOption1, itemSwitch2, switchOption2, itemSwitch3, switchOption3, itemSwitch4, switchOption4)

		else

			InitDLL(mainSetting, itemSwitch, switchOption1, itemSwitch, switchOption1, itemSwitch, switchOption1, itemSwitch4, switchOption4)

		endif

		actor playerRef = Game.GetPlayer()
 		Spell harvestSpell = Game.GetFormFromFile(0x08991, "AutoHarvest2.esp") as Spell
		if (playerRef && harvestSpell)
			bool hasHarvestSpell = playerRef.HasSpell(harvestSpell)
			if (mainSetting[22])
				if (!hasHarvestSpell)
					playerRef.AddSpell(harvestSpell)
				endif
			else
				if (hasHarvestSpell)
					playerRef.RemoveSpell(harvestSpell)
				endif
			endif
		endif

	else
		Debug.Notification("$unloadDLL")
		return
	endif
endFunction

Event OnConfigOpen()
	fissSetting = New Int[2]
endEvent

Event OnPageReset(String a_Page)
	If (a_Page == "")
		LoadCustomContent("towawot/AutoHarvest2.dds")
		Return
	Else
		UnloadCustomContent()
	EndIf

	if (a_Page == Pages[0])
; 	======================== LEFT ========================
		SetCursorFillMode(TOP_TO_BOTTOM)

		AddHeaderOption("$AH_generalHeader1")

		ID_mainSetting[0] = AddToggleOption(s_mainSetting[0], mainSetting[0] as bool)
		ID_mainSetting[9] = AddToggleOption(s_mainSetting[9], mainSetting[9] as bool)
		ID_mainSetting[10] = AddToggleOption(s_mainSetting[10], mainSetting[10] as bool)
		ID_mainSetting[3] = AddSliderOption(s_mainSetting[3], mainSetting[3], s1_mainSetting[3])
		ID_mainSetting[4] = AddSliderOption(s_mainSetting[4], mainSetting[4], s1_mainSetting[4])
		ID_mainSetting[11] = AddToggleOption(s_mainSetting[11], mainSetting[11] as bool)
		AddEmptyOption()

		AddHeaderOption("$AH_generalHeader2")

		ID_mainSetting[22] = AddToggleOption(s_mainSetting[22], mainSetting[22] as bool)
		ID_mainSetting[23] = AddSliderOption(s_mainSetting[23], mainSetting[23], s1_mainSetting[23])
		ID_mainSetting[24] = AddSliderOption(s_mainSetting[24], mainSetting[24], s1_mainSetting[24])

; 	======================== RIGHT ========================
		SetCursorPosition(1)
		AddHeaderOption("$AH_hotkeyHeader")
		ID_mainSetting[1] = AddToggleOption(s_mainSetting[1], mainSetting[1] as bool)
		ID_mainSetting[2] = AddKeyMapOption(s_mainSetting[2], mainSetting[2] as int)
		AddEmptyOption()
		AddEmptyOption()
		AddEmptyOption()
		AddEmptyOption()
		AddEmptyOption()
		AddHeaderOption("$AH_locationRegisterHeader")

		mainSetting[17] = CheckLocaltionInList("location")
		if (mainSetting[17] == -1.0)
			ID_mainSetting[17] = AddTextOption(s_mainSetting[17], _registerText[0], OPTION_FLAG_DISABLED)
		else
			string locName = _playerLoc.GetName()
			if (locName == "")
				locName = s_mainSetting[17]
			else
				locName = "LOC:" + locName
			endif
			
; 			if (mainSetting[17])
; 				ID_mainSetting[17] = AddTextOption(locName, _registerText[mainSetting[17] as int])
; 			else
				ID_mainSetting[17] = AddTextOption(locName, _registerText[mainSetting[17] as int])
; 			endif
		endIf

		mainSetting[18] = CheckLocaltionInList("cell")
		if (mainSetting[18] == -1.0)
			ID_mainSetting[18] = AddTextOption(s_mainSetting[18], _registerText[0], OPTION_FLAG_DISABLED)
		else
			string cellName = _playerCell.GetName()
			if (cellName == "")
				cellName = s_mainSetting[18]
			else
				cellName = "CELL:" + cellName
			endif
; 			if (mainSetting[18])
; 				ID_mainSetting[18] = AddTextOption(cellName, _registerText[mainSetting[18] as int])
; 			else
				ID_mainSetting[18] = AddTextOption(cellName, _registerText[mainSetting[18] as int])
; 			endIf
		endif

		ID_mainSetting[20] = AddToggleOption(s_mainSetting[20], mainSetting[20] as bool)

		
	elseif (a_Page == Pages[1])
; 	======================== LEFT ========================
		SetCursorFillMode(TOP_TO_BOTTOM)

		AddHeaderOption("$AH_FISSHeader")

		int FISSflags = OPTION_FLAG_NONE
		if (!IsLoadedFISS())
			FISSflags = OPTION_FLAG_DISABLED
		endif

		ID_FissPresetNumber[0] = AddSliderOption("$AH_presetNumber", fissPresetNumber[0], s1_fissPresetNumber[0])
		ID_Fiss[0] = AddToggleOption("$AH_savePersonalPreset", fissSetting[0] as Bool, FISSflags)
		ID_Fiss[1] = AddToggleOption("$AH_loadPersonalPreset", fissSetting[1] as Bool, FISSflags)

		AddEmptyOption()
		AddEmptyOption()
		AddEmptyOption()

		AddHeaderOption("$AH_fillterHeader")
		ID_mainSetting[7] = AddToggleOption(s_mainSetting[7], mainSetting[7] as bool)
		ID_mainSetting[12] = AddToggleOption(s_mainSetting[12], mainSetting[12] as bool)
		ID_mainSetting[15] = AddToggleOption(s_mainSetting[15], mainSetting[15] as bool)
		ID_mainSetting[8] = AddToggleOption(s_mainSetting[8], mainSetting[8] as bool)
		
; 	======================== RIGHT ========================
		SetCursorPosition(1)

		AddHeaderOption("$AH_optionHeader")
		ID_mainSetting[5] = AddTextOption(s_mainSetting[5], _ownershipText[mainSetting[5] as int])
		ID_mainSetting[21] = AddTextOption(s_mainSetting[21], _ownershipText[mainSetting[21] as int])
		ID_mainSetting[6] = AddTextOption(s_mainSetting[6], _questItemText[mainSetting[6] as int])
		ID_mainSetting[13] = AddTextOption(s_mainSetting[13], _inCombatText[mainSetting[13] as int])
		ID_mainSetting[19] = AddTextOption(s_mainSetting[19], _openContainerText[mainSetting[19] as int])
		AddEmptyOption()
		AddEmptyOption()
		AddEmptyOption()

		AddHeaderOption("$AH_optionHeader")
		ID_mainSetting[14] = AddToggleOption(s_mainSetting[14], mainSetting[14] as bool)
		ID_mainSetting[16] = AddToggleOption(s_mainSetting[16], mainSetting[16] as bool)

	elseif (a_Page == Pages[2])

		SetCursorFillMode(TOP_TO_BOTTOM)
; 	======================== LEFT ========================

		AddHeaderOption("$AH_typeHeader")

		int index = 0
		int _max  = (s_itemCategory.length - 1)

		ID_switch[39] = AddTextOption("", _switchAllText[itemSwitch[39]])

		while (index < _max)
			if (index == 32) ; foodIngredient
				ID_switch[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch[index]], OPTION_FLAG_DISABLED)
			elseif (index == 35) ; ole vein
				ID_switch[index] = AddTextOption(s_itemCategory[index], _switchOleVeinText[itemSwitch[index]])
			else
				ID_switch[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch[index]])
			endif
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile

		SetCursorPosition(5)

		index = 0
		while (index < _max)
			if (!s_switchOption[index])
				ID_switchOption1[index] = AddEmptyOption()
			elseif (index == 32) ; foodIngredient
				ID_switchOption1[index] = AddSliderOption(s_switchOption[index], switchOption1[index], s1_switchOption[index], OPTION_FLAG_DISABLED)
			else
				ID_switchOption1[index] = AddSliderOption(s_switchOption[index], switchOption1[index], s1_switchOption[index])
			endif
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile
	elseif (a_Page == Pages[3])

		SetCursorFillMode(TOP_TO_BOTTOM)

		int flags = GetMCMFlags(mainSetting[11])

; 	======================== LEFT ========================

		AddHeaderOption("$AH_typeHeader")

		int index = 0
		int _max  = (s_itemCategory.length - 1)

		ID_switch2[39] = AddTextOption("", _switchAllText[itemSwitch2[39]], flags)
		
		while (index < _max)
			if (index == 32 || index == 35 || index < 4) ; foodIngredient oreVein
				ID_switch2[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch2[index]], OPTION_FLAG_DISABLED)
			elseif (mainSetting[11])
				ID_switch2[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch2[index]], flags)
			else
				ID_switch2[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch[index]], flags)
			endif
			
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile

		SetCursorPosition(5)

		index = 0
		while (index < _max)
			if (!s_switchOption[index])
				ID_switchOption2[index] = AddEmptyOption()
			elseif (index == 32 || index < 4) ; foodIngredient
				ID_switchOption2[index] = AddSliderOption(s_switchOption[index], switchOption2[index], s1_switchOption[index], OPTION_FLAG_DISABLED)
			else
				if (mainSetting[11])
					ID_switchOption2[index] = AddSliderOption(s_switchOption[index], switchOption2[index], s1_switchOption[index], flags)
				else
					ID_switchOption2[index] = AddSliderOption(s_switchOption[index], switchOption1[index], s1_switchOption[index], flags)
				endif
			endif
			
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile
	elseif (a_Page == Pages[4])

		SetCursorFillMode(TOP_TO_BOTTOM)

		int flags = GetMCMFlags(mainSetting[11])

; 	======================== LEFT ========================

		AddHeaderOption("$AH_typeHeader")

		int index = 0
		int _max  = (s_itemCategory.length - 1)

		ID_switch3[39] = AddTextOption("", _switchAllText[itemSwitch3[39]], flags)

		while (index < _max)
			
			if (index == 32 || index == 35 || index < 4) ; foodIngredient oreVein
				ID_switch3[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch3[index]], OPTION_FLAG_DISABLED)
			elseif (mainSetting[11])
				ID_switch3[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch3[index]], flags)
			else
				ID_switch3[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch[index]], flags)
			endif
			
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile

		SetCursorPosition(5)

		index = 0
		while (index < _max)
			if (!s_switchOption[index])
				ID_switchOption3[index] = AddEmptyOption()
			elseif (index == 32 || index < 4) ; foodIngredient
				ID_switchOption3[index] = AddSliderOption(s_switchOption[index], switchOption3[index], s1_switchOption[index], OPTION_FLAG_DISABLED)
			else
				if (mainSetting[11])
					ID_switchOption3[index] = AddSliderOption(s_switchOption[index], switchOption3[index], s1_switchOption[index], flags)
				else
					ID_switchOption3[index] = AddSliderOption(s_switchOption[index], switchOption1[index], s1_switchOption[index], flags)
				endif
			endif

			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile
	elseif (a_Page == Pages[5])

		SetCursorFillMode(TOP_TO_BOTTOM)
; 	======================== LEFT ========================

		AddHeaderOption("$AH_typeHeader")

		int index = 0
		int _max  = (s_itemCategory.length - 1)

		ID_switch4[39] = AddTextOption("", _switchAllText[itemSwitch4[39]])

		while (index < _max)
			if (index == 32) ; foodIngredient
				ID_switch4[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch4[index]], OPTION_FLAG_DISABLED)
			elseif (index == 35) ; ole vein
				ID_switch4[index] = AddTextOption(s_itemCategory[index], _switchOleVeinText[itemSwitch4[index]])
			else
				ID_switch4[index] = AddTextOption(s_itemCategory[index], _switchText[itemSwitch4[index]])
			endif
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile

		SetCursorPosition(5)

		index = 0
		while (index < _max)
			if (!s_switchOption[index])
				ID_switchOption4[index] = AddEmptyOption()
			elseif (index == 32) ; foodIngredient
				ID_switchOption4[index] = AddSliderOption(s_switchOption[index], switchOption4[index], s1_switchOption[index], OPTION_FLAG_DISABLED)
			else
				ID_switchOption4[index] = AddSliderOption(s_switchOption[index], switchOption4[index], s1_switchOption[index])
			endif
			index += 1

			if(!s_itemCategory[index])
				index = _max
			endif
		endWhile
	elseif (a_Page == Pages[6])
		userlistItems = New form[128]
		userlistFlag = New bool[128]
		ID_Userlist = New int[128]
		int maxArray = 0

		maxArray = FormlistToArray(Userlist, userlistItems)
		if (maxArray > 0)
			int i = 0
			while (i < maxArray)
				userlistFlag[i] = true
				string itemName = userlistItems[i].GetName()
				if (itemName == "")
					itemName = "$AH_unknownName"
				else
					itemName = addZero(i) + ":" + itemName
				endif
				ID_Userlist[i] = AddToggleOption(itemName, userlistFlag)
				i += 1
			endWhile
		endif
		
; 		SetCursorFillMode(TOP_TO_BOTTOM)
	elseif (a_Page == Pages[7])
		exclusionlistItems = New form[128]
		exclusionlistFlag = New bool[128]
		ID_Exclusionlist = New int[128]
		int maxArray = 0
	
		maxArray = FormlistToArray(Exclusionlist, exclusionlistItems)
		if (maxArray > 0)
			int i = 0
			while (i < maxArray)
				exclusionlistFlag[i] = true
				string itemName = ""
				if (exclusionlistItems[i] as ObjectReference)
					itemName = (exclusionlistItems[i] as ObjectReference).GetDisplayName()
					if (itemName == "")
						itemName = "$AH_unknownName"
					else
						itemName = "Ref" + ":" + itemName
					endif
				elseif (exclusionlistItems[i])
					itemName = exclusionlistItems[i].GetName()
					if (itemName == "")
						itemName = "$AH_unknownName"
					else
						itemName = addZero(i) + ":" + itemName
					endif
				endif
				ID_Exclusionlist[i] = AddToggleOption(itemName, exclusionlistFlag)
				i += 1
			endWhile
		endif
; 		SetCursorFillMode(TOP_TO_BOTTOM)
	elseif (a_Page == Pages[8])
		locationlistItems = New form[128]
		locationlistFlag = New bool[128]
		ID_Locationlist = New int[128]
		int maxArray = 0
	
		maxArray = FormlistToArray(locationlist, locationlistItems)
		if (maxArray > 0)
			int i = 0
			while (i < maxArray)
				locationlistFlag[i] = true
				string itemName = locationlistItems[i].GetName()
				if (itemName != "")
					if (locationlistItems[i] as location)
						itemName = "LOC" + ":" + itemName
					elseif (locationlistItems[i] as cell)
						itemName = "CELL" + ":" + itemName
					endif
				else
					if (locationlistItems[i] as location)
						itemName = "$AH_unknownNameLOC"
					elseif (locationlistItems[i] as cell)
						itemName = "$AH_unknownNameCELL"
					endif
				endif
				
				ID_Locationlist[i] = AddToggleOption(itemName, locationlistFlag)
				i += 1
			endWhile
		endif
; 		SetCursorFillMode(TOP_TO_BOTTOM)
	endif
endEvent

Event OnConfigClose()
	if(IsLoadedFISS())
		FISSInterface fiss = FISSFactory.getFISS()
		If (fiss)
			int _fiss = GetNumFISS(fissSetting)
			if (_fiss > 0)
				FissPreset(_fiss)
			endif
		endif
	endif
	
	ApplySetting()

	MQ.Update()
EndEvent

event OnOptionHighlight(int a_option)
	int index = -1
	
	index = ID_mainSetting.find(a_option)
	if (index > -1)
		SetInfoText(s_mainSettingDesc[index])
		return
	endif

	index = ID_switch.find(a_option)
	if (index > -1)
		SetInfoText(s_itemCategoryDesc[index])
		return
	endif

	index = ID_switch2.find(a_option)
	if (index > -1)
		SetInfoText(s_itemCategoryDesc[index])
		return
	endif

	index = ID_switch3.find(a_option)
	if (index > -1)
		SetInfoText(s_itemCategoryDesc[index])
		return
	endif

	index = ID_switch4.find(a_option)
	if (index > -1)
		SetInfoText(s_itemCategoryDesc[index])
		return
	endif
	
	index = ID_Fiss.find(a_option)
	if (index > -1)
		SetInfoText(s_fissSettingDesc[index])
		return
	endif

endEvent

Event OnOptionSelect(int a_option)
	int index = -1

	index = ID_mainSetting.find(a_option)
	if (index > -1)
		if (index == 5 || index == 21)
			int _temp = CycleTEXT(_ownershipText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _ownershipText[_temp])
			mainSetting[index] = _temp as float
		elseif (index == 6)
			int _temp = CycleTEXT(_questItemText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _questItemText[_temp])
			mainSetting[index] = _temp as float
		elseif (index == 13)
			int _temp = CycleTEXT(_inCombatText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _inCombatText[_temp])
			mainSetting[index] = _temp as float
		elseif (index == 17)
			int _temp = CycleTEXT(_registerText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _registerText[_temp])
			mainSetting[index] = _temp as float
			if (_playerLoc)
				if (_temp)
					LocationList.AddForm(_playerLoc)
; 					locationlistAdd(_playerLoc)
				else
					LocationList.RemoveAddedForm(_playerLoc)
; 					locationlistRemove(_playerLoc)
				endif
; 				Debug.trace("----------------" + LocationList.GetSize())
			endif
		elseif (index == 18)
			int _temp = CycleTEXT(_registerText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _registerText[_temp])
			mainSetting[index] = _temp as float
			if (_playerCell)
				if (_temp == 1)
					LocationList.AddForm(_playerCell)
; 					locationlistAdd(_playerCell)
				else
					LocationList.RemoveAddedForm(_playerCell)
; 					locationlistRemove(_playerCell)
				endif
; 				Debug.trace("----------------" + LocationList.GetSize())
			endif
		elseif (index == 19)
			int _temp = CycleTEXT(_openContainerText, mainSetting[index] as int)
			SetTextOptionValue(a_option, _openContainerText[_temp])
			mainSetting[index] = _temp as float
		else
			mainSetting[index] = toggleInt(mainSetting[index] as int) as float
			SetToggleOptionValue(a_option, mainSetting[index] as Bool, false)
		endif
		return
	endif

	index = ID_switch.find(a_option)
	if (index > -1)
		if (index == 39)
			bool continue = true
			continue = ShowMessage("$AH_setAllMessage", true, "$AH_yes", "$AH_no")
			if (continue)
				int i = 0
				While (i < index)
					if (itemSwitch[i] == 0)
						int _temp = 2
						SetTextOptionValue(a_option, _switchText[_temp])
						itemSwitch[i] = _temp
					endif
					i += 1
				endWhile
				itemSwitch[39] = 0
				ForcePageReset()
			endif
			return
		elseif (index == 35)
			int _temp = CycleTEXT(_switchOleVeinText, itemSwitch[index])
			SetTextOptionValue(a_option, _switchOleVeinText[_temp])
			itemSwitch[index] = _temp
			return
		else
			int _temp = CycleTEXT(_switchText, itemSwitch[index])
			SetTextOptionValue(a_option, _switchText[_temp])
			itemSwitch[index] = _temp
			return
		endif
	endif

	index = ID_switch2.find(a_option)
	if (index > -1)
		if (index == 39)
			bool continue = true
			continue = ShowMessage("$AH_setAllMessage", true, "$AH_yes", "$AH_no")
			if (continue)
				int i = 4
				While (i < index)
					if (itemSwitch2[i] == 0)
						int _temp = 2
						SetTextOptionValue(a_option, _switchText[_temp])
						itemSwitch2[i] = _temp
					endif
					i += 1
				endWhile
				itemSwitch2[39] = 0
				ForcePageReset()
			endif
			return
		else
			int _temp = CycleTEXT(_switchText, itemSwitch2[index])
			
			SetTextOptionValue(a_option, _switchText[_temp])
			itemSwitch2[index] = _temp
			return
		endif
	endif

	index = ID_switch3.find(a_option)
	if (index > -1)
		if (index == 39)
			bool continue = true
			continue = ShowMessage("$AH_setAllMessage", true, "$AH_yes", "$AH_no")
			if (continue)
				int i = 4
				While (i < index)
					if (itemSwitch3[i] == 0)
						int _temp = 2
						SetTextOptionValue(a_option, _switchText[_temp])
						itemSwitch3[i] = _temp
					endif
					i += 1
				endWhile
				itemSwitch3[39] = 0
				ForcePageReset()
			endif
			return
		else
			int _temp = CycleTEXT(_switchText, itemSwitch3[index])
			
			SetTextOptionValue(a_option, _switchText[_temp])
			itemSwitch3[index] = _temp
			return
		endif
	endif

	index = ID_switch4.find(a_option)
	if (index > -1)
		if (index == 39)
			bool continue = true
			continue = ShowMessage("$AH_setAllMessage", true, "$AH_yes", "$AH_no")
			if (continue)
				int i = 0
				While (i < index)
					if (itemSwitch4[i] == 0)
						int _temp = 2
						SetTextOptionValue(a_option, _switchText[_temp])
						itemSwitch4[i] = _temp
					endif
					i += 1
				endWhile
				itemSwitch4[39] = 0
				ForcePageReset()
			endif
			return
		elseif (index == 35)
			int _temp = CycleTEXT(_switchOleVeinText, itemSwitch4[index])
			SetTextOptionValue(a_option, _switchOleVeinText[_temp])
			itemSwitch4[index] = _temp
			return
		else
			int _temp = CycleTEXT(_switchText, itemSwitch4[index])
			SetTextOptionValue(a_option, _switchText[_temp])
			itemSwitch4[index] = _temp
			return
		endif
	endif
	
	index = ID_Userlist.find(a_option)
	if (index > -1)
		userlistFlag[index] = toggleInt(userlistFlag[index] as int) as bool
		SetToggleOptionValue(a_option, userlistFlag[index] as Bool, false)
		form item = userlistItems[index]
		if (item)
			if (userlistFlag[index])
				userlist.AddForm(item)
				UserlistAdd(item)
			else
				userlist.RemoveAddedForm(item)
				UserlistRemove(item)
			endif
		endif
		return
	endif

	index = ID_Exclusionlist.find(a_option)
	if (index > -1)
		ExclusionlistFlag[index] = toggleInt(ExclusionlistFlag[index] as int) as bool
		SetToggleOptionValue(a_option, ExclusionlistFlag[index] as Bool, false)
		form item = ExclusionlistItems[index]
		if (item)
			if (ExclusionlistFlag[index])
				Exclusionlist.AddForm(item)
				ExclusionlistAdd(item)
			else
				Exclusionlist.RemoveAddedForm(item)
				ExclusionlistRemove(item)
			endif
		endif
		return
	endif

	index = ID_Locationlist.find(a_option)
	if (index > -1)
		locationlistFlag[index] = toggleInt(locationlistFlag[index] as int) as bool
		SetToggleOptionValue(a_option, locationlistFlag[index] as Bool, false)
		form item = locationlistItems[index]
		if (item)
			if (locationlistFlag[index])
				locationlist.AddForm(item)
			else
				locationlist.RemoveAddedForm(item)
			endif
		endif
		return
	endif
	
	index = ID_Fiss.find(a_option)
	if (index > -1)
		bool continue = true
		if (fissSetting[index] == 0)
			continue = ShowMessage(setFissMessage(index), true, "$AH_yes", "$AH_no")
		endif
		
		if (continue)
			fissSetting[index] = toggleInt(fissSetting[index])
			SetToggleOptionValue(a_option, fissSetting[index] as Bool, false)

			int _subIndex = 1
			if (index == 1)
				_subIndex = 0
			endif
			if (fissSetting[_subIndex] > 0)
				fissSetting[_subIndex] = toggleInt(fissSetting[_subIndex])
				SetToggleOptionValue(ID_Fiss[_subIndex], fissSetting[_subIndex] as Bool, false)
			endif
		endif
		return
	endif
endEvent

event OnOptionKeyMapChange(int a_option, int keyCode, string conflictControl, string conflictName)
	int index = -1

	index = ID_mainSetting.find(a_option)
	if (index > -1)
		SetKeyMapOptionValue(ID_mainSetting[index], keyCode)
		mainSetting[index] = keyCode
		return
	endif
endEvent

event OnOptionSliderOpen(int a_option)
	int index = -1

	index = ID_mainSetting.find(a_option)
	if (index > -1)
		if (index == 3)
			SetSliderDialogStartValue(mainSetting[index])
			SetSliderDialogDefaultValue(300)
			SetSliderDialogRange(50, 1000)
			SetSliderDialogInterval(5)
		elseif (index == 4)
			SetSliderDialogStartValue(mainSetting[index])
			SetSliderDialogDefaultValue(0.2)
			SetSliderDialogRange(0.1, 5)
			SetSliderDialogInterval(0.1)
		elseif (index == 23)
			SetSliderDialogStartValue(mainSetting[index])
			SetSliderDialogDefaultValue(5000)
			SetSliderDialogRange(2000, 20000)
			SetSliderDialogInterval(100)
		elseif (index == 24)
			SetSliderDialogStartValue(mainSetting[index])
			SetSliderDialogDefaultValue(1)
			SetSliderDialogRange(0.8, 5)
			SetSliderDialogInterval(0.2)
		endif
		return
	endif
	
	index = ID_switchOption1.find(a_option)
	if (index > -1)
		if (index == 20) ;ammo
			SetSliderDialogStartValue(switchOption1[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 30)
			SetSliderDialogInterval(1.0)
		else
			SetSliderDialogStartValue(switchOption1[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 500)
			SetSliderDialogInterval(0.1)
		endif
		return
	endif

	index = ID_switchOption2.find(a_option)
	if (index > -1)
		if (index == 20) ;ammo
			SetSliderDialogStartValue(switchOption2[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 30)
			SetSliderDialogInterval(1.0)
		else
			SetSliderDialogStartValue(switchOption2[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 500)
			SetSliderDialogInterval(0.1)
		endif
		return
	endif

	index = ID_switchOption3.find(a_option)
	if (index > -1)
		if (index == 20) ;ammo
			SetSliderDialogStartValue(switchOption3[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 30)
			SetSliderDialogInterval(1.0)
		else
			SetSliderDialogStartValue(switchOption3[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 500)
			SetSliderDialogInterval(0.1)
		endif
		return
	endif

	index = ID_switchOption4.find(a_option)
	if (index > -1)
		if (index == 20) ;ammo
			SetSliderDialogStartValue(switchOption4[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 30)
			SetSliderDialogInterval(1.0)
		else
			SetSliderDialogStartValue(switchOption4[index])
			SetSliderDialogDefaultValue(0)
			SetSliderDialogRange(0, 500)
			SetSliderDialogInterval(0.1)
		endif
		return
	endif
	
	index = ID_FissPresetNumber.find(a_option)
	if (index > -1)
		SetSliderDialogStartValue(fissPresetNumber[index])
		SetSliderDialogDefaultValue(0)
		SetSliderDialogRange(0, 9)
		SetSliderDialogInterval(1)
		return
	endif
endEvent

event OnOptionSliderAccept(int a_option, float a_value)
	int index = -1

	index = ID_mainSetting.find(a_option)
	if (index > -1)
		mainSetting[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_mainSetting[index])
		return
	endif
	
	index = ID_switchOption1.find(a_option)
	if (index > -1)
		switchOption1[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_switchOption[index])
		return
	endif

	index = ID_switchOption2.find(a_option)
	if (index > -1)
		switchOption2[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_switchOption[index])
		return
	endif

	index = ID_switchOption3.find(a_option)
	if (index > -1)
		switchOption3[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_switchOption[index])
		return
	endif
	
	index = ID_switchOption4.find(a_option)
	if (index > -1)
		switchOption4[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_switchOption[index])
		return
	endif

	index = ID_FissPresetNumber.find(a_option)
	if (index > -1)
		fissPresetNumber[index] = a_value
		SetSliderOptionValue(a_option, a_value, s1_fissPresetNumber[index])
		return
	endif
endEvent

float Function CheckLocaltionInList(string s)
	bool result
	if (s == "location")
		_playerLoc = Game.GetPlayer().GetCurrentLocation()
		if (!_playerLoc)
			return -1.0
		else
			result = LocationList.HasForm(_playerLoc)
		endif
	else
		_playerCell = Game.GetPlayer().GetParentCell()
		if (!_playerCell)
			return -1.0
		else
			result = LocationList.HasForm(_playerCell)
		endif
	endif

	if (result)
		return 1.0
	endif
	return 0.0
endFunction

function FissPreset(int value)
	if (!IsLoadedFISS())
		return
	else
		FISSInterface fiss = FISSFactory.getFISS()
		If (fiss)
			int index = 0
			int _max  = s_itemCategory.length
			string str
			if (value == 1) ; save
			
				if (fissPresetNumber[0] == 0.0)
					fiss.beginSave(ModName + ".xml", ModName)
				else
					int presetNum = fissPresetNumber[0] as int
					fiss.beginSave(ModName + "_" + presetNum + ".xml", ModName)
				endif
				
				while (index < _max)
					str = "_general" + addZero(index)
					fiss.saveFloat(str, mainSetting[index])

					str = "_switch" + addZero(index)
					fiss.saveInt(str, itemSwitch[index])
					str = "_option1" + addZero(index)
					fiss.saveFloat(str, switchOption1[index])

					str = "_switch2" + addZero(index)
					fiss.saveInt(str, itemSwitch2[index])
					str = "_option2" + addZero(index)
					fiss.saveFloat(str, switchOption2[index])

					str = "_switch3" + addZero(index)
					fiss.saveInt(str, itemSwitch3[index])
					str = "_option3" + addZero(index)
					fiss.saveFloat(str, switchOption3[index])

					str = "_switch4" + addZero(index)
					fiss.saveInt(str, itemSwitch4[index])
					str = "_option4" + addZero(index)
					fiss.saveFloat(str, switchOption4[index])

					index += 1
				endWhile
				
				string _result = fiss.endSave()
				if (!_result)
					Debug.Notification("$AH_presetSaved")
				else
					Debug.Notification(_result)
				endif
			elseif (value == 2) ; load

				if (fissPresetNumber[0] == 0.0)
					fiss.beginLoad(ModName + ".xml")
				else
					int presetNum = fissPresetNumber[0] as int
					fiss.beginLoad(ModName + "_" + presetNum + ".xml")
				endif
				
				while (index < _max)
					str = "_general" + addZero(index)
					mainSetting[index] = fiss.loadFloat(str)

					str = "_switch" + addZero(index)
					itemSwitch[index] = fiss.loadInt(str)
					str = "_option1" + addZero(index)
					switchOption1[index] = fiss.loadFloat(str)

					str = "_switch2" + addZero(index)
					itemSwitch2[index] = fiss.loadInt(str)
					str = "_option2" + addZero(index)
					switchOption2[index] = fiss.loadFloat(str)

					str = "_switch3" + addZero(index)
					itemSwitch3[index] = fiss.loadInt(str)
					str = "_option3" + addZero(index)
					switchOption3[index] = fiss.loadFloat(str)

					str = "_switch4" + addZero(index)
					itemSwitch4[index] = fiss.loadInt(str)
					str = "_option4" + addZero(index)
					switchOption4[index] = fiss.loadFloat(str)
					
					index += 1
				endWhile
				string _result = fiss.endLoad()
				if (!_result)
					Debug.Notification("$AH_presetLoaded")
				else
					Debug.Notification(_result)
				endif
			endif
		endif
	endif
endFunction

string function setFissMessage(int value)
	if (value == 0)
		return "$AH_saveFISS"
	elseif (value == 1)
		return "$AH_loadFISS"
	endif
	return ""
endFunction

int Function GetNumFISS(int[] arrays)
	if (arrays[0])
		return 1
	elseif (arrays[1])
		return 2
	endif
	return 0
endFunction


int Function CycleTEXT(string[] s, int num)
	int index = num + 1
	if (index >= s.length)
		index = 0
	endif
	return index
endFunction

int Function GetMCMFlags(float var)
	int result = OPTION_FLAG_NONE
	if (var == 0.0)
		result = OPTION_FLAG_DISABLED
	endif
	return result
endFunction

int function toggleInt(int value, int result = 1)
	if (value)
		return 0
	endif
	return result
endFunction

string Function addZero(int num)
	string s = num as string
	string _result
	if (num < 10)
		_result = "0" + s
	else
		_result = s
	endif
	return _result
endFunction
